import hashlib
import json


def cache_to_file(file_name):

    def decorator(function):

        def function_wrapped(*args, **kwargs):
            # Try to load cache from file
            cache = {}
            args_digest = hashlib.sha256(repr(args).encode()).hexdigest()
            kwargs_digest = hashlib.sha256(repr(kwargs).encode()).hexdigest()
            try:
                cache = json.load(open(file_name, 'r'))
            except FileNotFoundError:
                cache[args_digest] = {}
                cache[args_digest][kwargs_digest] = function(*args, **kwargs)
                json.dump(cache, open(file_name, 'w'))

            # Select relevant value from cache
            try:
                return_value = cache[args_digest][kwargs_digest]
            except KeyError:
                try:
                    cache[args_digest][kwargs_digest] = function(*args, **kwargs)
                    json.dump(cache, open(file_name, 'w'))

                    return_value = cache[args_digest][kwargs_digest]
                except KeyError:
                    cache[args_digest] = {}
                    cache[args_digest][kwargs_digest] = function(*args, **kwargs)
                    json.dump(cache, open(file_name, 'w'))

                    return_value = cache[args_digest][kwargs_digest]

            return return_value

        return function_wrapped

    return decorator
