from pathlib import Path

ROOT_PATH: Path = Path(__file__).parent.parent.parent
CACHE_PATH: Path = ROOT_PATH / '.cache'
