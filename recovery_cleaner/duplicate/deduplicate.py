import hashlib
import os
import time
from multiprocessing.managers import DictProxy
from multiprocessing import Pool, Manager
from multiprocessing.pool import Pool
from random import choice
from typing import List, Dict

from send2trash import send2trash

from recovery_cleaner.configuration import Configuration
from recovery_cleaner.get_file_path_strings import get_file_path_strings
from recovery_cleaner.util.cache_to_file import cache_to_file
from recovery_cleaner.util.paths import CACHE_PATH


def digest_file(file_path: str) -> str:
    try:
        with open(file_path, 'rb') as file:
            this_file_as_bytes = file.read()
    except FileNotFoundError:
        if os.path.islink(file_path):
            return ""
        else:
            raise NotImplementedError()

    file_digest: str = \
        hashlib.sha256(this_file_as_bytes).hexdigest()

    return file_digest


# @cache_to_file(CACHE_PATH / 'digest_files.cache')
def get_file_paths_to_digests_manager(
    file_path_strings: List[str],
) -> Dict[str, str]:
    pool: Pool = Pool(processes=Configuration.process_count)
    manager: Manager = Manager()
    file_paths_to_digests: DictProxy = manager.dict()

    for file_path in file_path_strings:
        pool.apply_async(
            get_file_digest,
            args=[[file_paths_to_digests, file_path]]
        )

    pool.close()
    pool.join()

    # Stop managing and thus make serializable before returning
    file_paths_to_digests_dict: Dict = dict(
        file_paths_to_digests)

    return file_paths_to_digests_dict


def get_file_digest(file_paths_to_digests_and_file_path):
    file_paths_to_digests, file_path = \
        file_paths_to_digests_and_file_path

    digest = digest_file(file_path=file_path)
    file_paths_to_digests.update({file_path: digest})


@cache_to_file(CACHE_PATH / 'get_file_paths_to_file_digests.cache')
def get_file_paths_to_file_digests(folder_path: str) -> Dict[str, str]:
    file_path_strings = \
        get_file_path_strings(folder_path=folder_path)

    print_file_path_strings(file_path_strings)

    print(
        f'Digesting files in {Configuration.process_count} processes ...'
    )
    start = time.time()
    file_paths_to_digests: Dict[str, str] = \
        get_file_paths_to_digests_manager(
            file_path_strings=file_path_strings,
        )
    print(
        f'Digesting {len(file_paths_to_digests)} files '
        f'took {time.time() - start} seconds.'
    )

    print_file_digests(file_paths_to_digests)

    return file_paths_to_digests


def print_file_digests(file_paths_to_digests):
    print(f'Digest count: {len(file_paths_to_digests.keys())}')
    example_index = choice(range(len(file_paths_to_digests)))
    example_path = list(file_paths_to_digests.keys())[example_index]
    example_digest = file_paths_to_digests[example_path]
    print(
        f'Example: '
        f'{example_index} - {example_digest} - {example_path}'
    )


def print_file_path_strings(file_paths):
    print(f'File count: {len(file_paths)}')

    example_index = choice(range(len(file_paths)))
    example_value = file_paths[example_index]
    print(f'Example: {example_value}')


def print_deletion(
    backup_file_digest: str,
    backup_file_path_string: str,
    home_file_path: str,
):
    print(
        f'Would delete "{backup_file_digest}" '
        f'at {backup_file_path_string} '
        f'(as known from {home_file_path}).'
    )


def double_check_deletion(
    backup_file_digest: str,
    backup_file_path_string: str,
    home_file_path: str,
):
    # Double check backup file digest
    digest_backup = digest_file(file_path=backup_file_path_string)
    if digest_backup != backup_file_digest:
        raise ValueError()

    # Double check home file path
    digest_home = digest_file(file_path=home_file_path)
    if digest_home != backup_file_digest or \
            digest_home != digest_backup:
        raise ValueError()

    print('Clear for deletion.')


def trash_backup_file(backup_file_path_string: str):
    print(f'Trashing file {backup_file_path_string}.')
    send2trash(paths=backup_file_path_string)


def delete_backup_file(backup_file_path_string: str):
    if os.path.exists(path=backup_file_path_string):
        print(
            f'WARN: Deleting file {backup_file_path_string}. '
            f'This should not be necessary.'
        )
        os.remove(path=backup_file_path_string)
    else:
        print("INFO: The file to delete does not exist, which is fine.")


def deduplicate_files_in_backup():
    home_file_paths_to_digests = \
        get_file_paths_to_file_digests(
            folder_path=Configuration.home_folder_path,
        )
    backup_file_paths_to_digests = \
        get_file_paths_to_file_digests(
            folder_path=Configuration.backup_folder_path,
        )
    for backup_file_path_string, backup_file_digest in \
            backup_file_paths_to_digests.items():
        for home_file_path_string, home_file_digest in \
                home_file_paths_to_digests.items():
            if backup_file_digest != home_file_digest:
                continue

            print_deletion(
                backup_file_digest=backup_file_digest,
                backup_file_path_string=backup_file_path_string,
                home_file_path=home_file_path_string,
            )

            double_check_deletion(
                backup_file_digest=backup_file_digest,
                backup_file_path_string=backup_file_path_string,
                home_file_path=home_file_path_string,
            )

            # trash_backup_file(
            #    backup_file_path_string=backup_file_path_string,
            # )

            # delete_backup_file(
            #     backup_file_path_string=backup_file_path_string,
            # )
