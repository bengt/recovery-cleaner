import subprocess
from multiprocessing import Pool, Manager
from multiprocessing.managers import DictProxy
from typing import Dict
from subprocess import Popen

from recovery_cleaner.deduplicate import trash_backup_file
from recovery_cleaner.util.cache_to_file import cache_to_file
from recovery_cleaner.configuration import Configuration
from recovery_cleaner.get_file_path_strings import get_file_path_strings
from recovery_cleaner.util.remove_prefix import remove_prefix
from recovery_cleaner.util.paths import CACHE_PATH


def delete_unusable_images():
    backup_file_path_strings = \
        get_file_path_strings(folder_path=Configuration.backup_folder_path)

    return_codes_errors_outputs = identify_files(backup_file_path_strings)

    for (file_path, (return_code, error, output)) in \
            return_codes_errors_outputs.items():
        # print(f'Checking file {file_path}')
        delete_image_if_unusable(
            error=error,
            output=output,
            return_code=return_code,
            file_path=file_path,
        )


@cache_to_file(CACHE_PATH / 'identify_files.cache')
def identify_files(backup_file_path_strings):
    pool: Pool = Pool(processes=Configuration.process_count)
    manager: Manager = Manager()
    return_codes_errors_outputs: DictProxy = manager.dict()
    for file_path in backup_file_path_strings:
        if not file_path.startswith(
            '/media/bengt/Home-Backup-1TB/Data Recovery 2021-09-08 at '
            '11.14.27/Graphic/'
        ):
            continue

        pool.apply_async(
            identify_file,
            args=[[return_codes_errors_outputs, file_path, ]]
        )
    pool.close()
    pool.join()

    print(f'Got {len(return_codes_errors_outputs)} identifications.')

    # Stop managing and thus make serializable before returning
    return_codes_errors_outputs: Dict = dict(return_codes_errors_outputs)

    return return_codes_errors_outputs


def delete_image_if_unusable(return_code, error, output, file_path):
    error = remove_prefix(text=error, prefix='identify-im6.q16: ')

    if return_code == 0:
        if error == '':
            if output == '':
                return
            raise NotImplementedError(return_code, error, output)
        if error.startswith(
            'Corrupt JPEG data'
        ) or error.startswith(
            'Premature end of JPEG file'
        ) or error.startswith(
            'Invalid SOS parameters for sequential JPEG'
        ) or error.startswith(
            'sRGB: out of place'
        ) or error.startswith(
            'iCCP: extra compressed data'
        ) or error.startswith(
            'iCCP: too many profiles'
        ) or error.startswith(
            'cHRM: inconsistent chromaticities'
        ) or error.startswith(
            "iCCP: profile 'ICC Profile': 1000000h: invalid rendering intent"
        ) or error.startswith(
            "iCCP: profile 'ASUS PG279Q Color Profile,D6500': 0h: PCS illuminant is not D50"
        ) or error.startswith(
            'cHRM: invalid chromaticities'
        ) or error.startswith(
            'sBIT: invalid'
        ) or error.startswith(
            "iCCP: profile 'icc': 0h: PCS illuminant is not D50"
        ) or error.startswith(
            'Incorrect value for "RichTIFFIPTC"; tag ignored.'
        ) or error.startswith(
            'Wrong data type 9 for "PixelXDimension"; tag ignored.'
        ):
            print_deletion(error=error, file_path=file_path)
            trash_backup_file(backup_file_path_string=file_path)
            return
        raise NotImplementedError(return_code, error, output)

    # Special case: Error unknown to imagemagick
    if return_code == 1 and error == '' and output == '':
        print_deletion(error=error, file_path=file_path)
        trash_backup_file(backup_file_path_string=file_path)
        return

    if error.startswith(
        'insufficient image data in file'
    ) or error.startswith(
        'negative or zero image size'
    ) or error.startswith(
        'length and filesize do not match'
    ) or error.startswith(
        'static planes value not equal to 1'
    ) or error.startswith(
        'unrecognized compression'
    ) or error.startswith(
        'delegate failed'
    ) or error.startswith(
        'corrupt image'
    ) or error.startswith(
        'invalid colormap index'
    ) or error.startswith(
        'memory allocation failed'
    ) or error.startswith(
        'Premature end of JPEG file'
    ) or error.startswith(
        'Corrupt JPEG data'
    ) or error.startswith(
        'width or height exceeds limit'
    ) or error.startswith(
        'Bogus Huffman table definition'
    ) or error.startswith(
        'Unsupported marker type'
    ) or error.startswith(
        'cache resources exhausted'
    ) or error.startswith(
        'unable to decompress image'
    ) or error.startswith(
        "Can not read TIFF directory count. `TIFFFetchDirectory'"
    ) or error.startswith(
        'Failed to allocate memory for to read TIFF directory'
    ) or error.startswith(
        'Can not read TIFF directory.'
    ) or error.startswith(
        'Incorrect value for "RichTIFFIPTC"; tag ignored.'
    ) or error.startswith(
        'Read error at scanline'
    ) or error.startswith(
        'Read error on strip'
    ) or error.startswith(
        'Wrong length of decoded string: data probably corrupted at scanline'
    ) or error.startswith(
        'Decoding error at scanline'
    ):
        print_deletion(error=error, file_path=file_path)
        trash_backup_file(backup_file_path_string=file_path)
        return
    raise NotImplementedError(return_code, error, output)


def print_deletion(error, file_path):
    error_string = error.rstrip().split('\n')[0]
    print(
        f'Would delete unusable image {file_path}. '
        f'Reason: {error_string}'
        # .split(Configuration.backup_folder_path.encode())}'
    )


def identify_file(return_codes_stderrs_stdout_and_file_path):
    return_codes_errors_outputs, file_path = \
        return_codes_stderrs_stdout_and_file_path

    command: str = f'identify -verbose "{file_path}" > /dev/null'

    process: Popen = subprocess.Popen(
        command,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    return_code = process.wait()
    output, error = process.communicate()

    error = error.decode('utf8')
    output = output.decode('utf8')

    return_codes_errors_outputs[file_path] = (return_code, error, output)
