from PIL import Image

from recovery_cleaner.configuration import Configuration
from recovery_cleaner.get_file_path_strings import get_file_path_strings


def is_nautilus_thumbnail(file_path_string):
    if not file_path_string.endswith('.png') and \
            not file_path_string.endswith('.PNG'):
        # Nautilus thumbnails are PNGs, so this cannot be one.
        return False

    image: Image = Image.open(file_path_string)

    if not (image.width == 128 and image.height == 128):
        if not (image.width == 256 and image.height == 256):
            # Nautilus only does these two sizes.
            return False

    return True


def delete_thumbnail_images():
    backup_file_path_strings = \
        get_file_path_strings(folder_path=Configuration.backup_folder_path)

    for backup_file_path_string in backup_file_path_strings:
        if is_nautilus_thumbnail(file_path_string=backup_file_path_string):
            print(f'Found a thumbnail: {backup_file_path_string}')
            break
