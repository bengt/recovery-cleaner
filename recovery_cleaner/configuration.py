from typing import final, Final


@final
class Configuration:
    canonical_file_name_extensions = (
        '.jpg',
        '.JPG',
        '.png',
        '.PNG',
    )
    process_count: Final[int] = 32
    home_folder_path: Final[str] = \
        u'/home/bengt'
    home_images_folder_path: Final[str] = \
        u'/home/bengt/Bilder'
    backup_folder_path: Final[str] = \
        u'/media/bengt/Home-Backup-1TB/Data Recovery 2021-09-08 at 11.14.27'
