import os
from pathlib import Path

from recovery_cleaner.configuration import Configuration
from recovery_cleaner.get_file_path_strings import get_file_path_strings
from recovery_cleaner.own_photos.is_own_photo import is_own_photo


def move_own_photos():
    backup_file_path_strings = \
        get_file_path_strings(folder_path=Configuration.backup_folder_path)

    own_photos_path = Path(Configuration.backup_folder_path) / 'own_photos'
    try:
        os.mkdir(own_photos_path)
    except FileExistsError:
        pass

    for backup_file_path_string in backup_file_path_strings:
        if is_own_photo(file_path_string=backup_file_path_string):
            filename = os.path.basename(backup_file_path_string)
            print(f'Found own photo: {backup_file_path_string}')

            destination = own_photos_path / filename
            source = backup_file_path_string

            # print_move(destination, source)

            os.rename(
                src=source,
                dst=destination,
            )


def print_move(destination, source):
    print(
        f'Moving file:\n'
        f'\tfrom {source}\n'
        f'\tto {destination}'
    )
