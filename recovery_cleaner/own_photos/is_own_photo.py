from PIL import Image, ExifTags


def is_own_photo(file_path_string):
    if not file_path_string.endswith('.jpg') and \
            not file_path_string.endswith('.jpeg') and \
            not file_path_string.endswith('.JPG') and \
            not file_path_string.endswith('.JPEG'):
        # Own photos are JPGs, so this cannot be one.
        return False

    image: Image = Image.open(file_path_string)
    exif: ExifTags = image.getexif()

    if exif == {}:
        # Nautilus thumbnails have fields in their exif data.
        return False

    # My Samsung S9
    if 271 in exif and exif[271] == 'samsung':
        if 272 in exif and exif[272] == 'SM-G960F':
            return True

    # Nadia's Samsung S8
    if 271 in exif and exif[271] == 'samsung':
        if 272 in exif and exif[272] == 'SM-G950F':
            return True

    # My first camera
    if 271 in exif and exif[271] == 'FUJIFILM':
        if 272 in exif and exif[272] == 'FinePixA201':
            return True

    # Peter's camera
    if 271 in exif and exif[271] == 'FUJIFILM':
        if 272 in exif and exif[272] == 'FinePix F20    ':
            return True

    # My second camera
    if 271 in exif and exif[271] == 'Canon':
        if 272 in exif and exif[272] == 'Canon PowerShot A650 IS':
            return True

    # Birger's camera
    if 271 in exif and exif[271] == 'Canon':
        if 272 in exif and exif[272] == 'Canon PowerShot S5 IS':
            return True

    # Nastja's guest family's friend's camera
    if 271 in exif and exif[271] == 'Canon':
        if 272 in exif and exif[272] == 'Canon PowerShot G11':
            return True

    # Nastja's old smartphone
    if 271 in exif and exif[271] == 'Apple':
        if 272 in exif and exif[272] == 'iPhone SE':
            return True

    # A PG mate's smartphone
    if 271 in exif and exif[271] == 'Apple':
        if 272 in exif and exif[272] == 'iPhone 4S':
            return True

    # A PG mate's smartphone
    if 271 in exif and exif[271] == 'Apple':
        if 272 in exif and exif[272] == 'iPhone 5':
            return True

    # My LG G4
    if 271 in exif and exif[271] == 'LGE':
        if 272 in exif and exif[272] == 'LG-H815':
            return True

    # My LG G4
    if 271 in exif and exif[271] == 'LG Electronics':
        if 272 in exif and exif[272] == 'LG-H815':
            return True

    # Nastja's HTC Desire
    if 271 in exif and exif[271].startswith('HTC'):
        if 272 in exif and exif[272].startswith('Desire'):
            return True

    # Nastja's HTC Desire
    if 271 in exif and exif[271].startswith('HTC'):
        if 272 in exif and exif[272].startswith('HTC Desire'):
            return True

    # My HTC Sensation
    if 271 in exif and exif[271].startswith('HTC'):
        if 272 in exif and exif[272].startswith('Sensation'):
            return True

    # My Moto G
    if 271 in exif and exif[271].startswith('Motorola'):
        if 272 in exif and exif[272].startswith('XT1032'):
            return True

    # Nastja's Moto G
    if 271 in exif and exif[271].startswith('Motorola'):
        if 272 in exif and exif[272].startswith('Moto G'):
            return True

    # Nastja's camera
    if 271 in exif and exif[271] == 'NIKON':
        if 272 in exif and exif[272] == 'COOLPIX P50  ':
            return True

    # My schoolmate's camera
    if 271 in exif and exif[271] == '3M DSC':
        if 272 in exif and exif[272] == 'DZ358':
            return True

    # My schoolmate's camera
    if 271 in exif and exif[271].startswith('SONY'):
        if 272 in exif and exif[272].startswith('DSLR-A100'):
            return True

    # My schoolmate's camera
    if 271 in exif and exif[271].startswith('RICOH'):
        if 272 in exif and exif[272].startswith('Caplio RR30'):
            return True

    # My schoolmate's camera
    if 271 in exif and exif[271].startswith('Digital'):
        if 272 in exif and exif[272].startswith('Digital'):
            return True

    # My granddad's camera
    if 271 in exif and exif[271].startswith('Hewlett-Packard Company'):
        if 272 in exif and exif[272].startswith('PhotoSmart C200'):
            return True

    # My schoolmate's camera
    if 271 in exif and exif[271] == 'CAMERA':
        if 272 in exif and exif[272] == '4MP-9T2':
            return True

    # My photographer's camera
    if 271 in exif and exif[271] == 'NIKON CORPORATION':
        if 272 in exif and exif[272] == 'NIKON D750':
            return True

    # Nastja's friend's camera
    if 271 in exif and exif[271] == 'Panasonic':
        if 272 in exif and exif[272] == 'DMC-FZ38':
            return True

    # Nastja's old feature phone
    if 271 in exif and exif[271] == 'Motorola':
        if 272 in exif and exif[272] == 'Phone':
            return True

    # Nastja's parent's smartphone
    if 271 in exif and exif[271] == 'Xiaomi':
        if 272 in exif and exif[272] == 'Redmi Note 4':
            return True

    # Unknown digital camera
    if 271 in exif and exif[271] == 'DIGITAL CAMERA     ':
        if 272 in exif and exif[272] == 'CAMERA':
            return True

    # Extinction rebellion's digital camera
    if 271 in exif and exif[271] == 'NIKON CORPORATION':
        if 272 in exif and exif[272] == 'NIKON D5100':
            return True

    # Astrid's smartphone
    if 271 in exif and exif[271].startswith('Fairphone'):
        if 272 in exif and exif[272].startswith('FP1'):
            return True

    # A school mate's digital camera
    if 271 in exif and exif[271] == 'SONY':
        if 272 in exif and exif[272] == 'DSLR-A100':
            return True

    # A school mate's digital camera
    if 271 in exif and exif[271] == 'CASIO COMPUTER CO.,LTD ':
        if 272 in exif and exif[272] == 'EX-Z4  ':
            return True

    # A school mate's digital camera
    if 271 in exif and exif[271] == 'CASIO COMPUTER CO.,LTD.':
        if 272 in exif and exif[272] == 'QV-R4  ':
            return True

    # A school mate's digital camera
    if 271 in exif and exif[271] == 'CASIO COMPUTER CO.,LTD.':
        if 272 in exif and exif[272] == 'EX-Z500':
            return True

    # A school mate's digital camera
    if 271 in exif and exif[271] == 'Canon':
        if 272 in exif and exif[272] == 'Canon DIGITAL IXUS 500':
            return True

    # Dennis' smartphone
    if 271 in exif and exif[271] == 'samsung':
        if 272 in exif and exif[272] == 'SM-A520F':
            return True

    # Annika's colleague's digital camera
    if 271 in exif and exif[271].startswith('HTC'):
        if 272 in exif and exif[272].startswith('HTC One A9s'):
            return True

    print(file_path_string, exif)

    return False