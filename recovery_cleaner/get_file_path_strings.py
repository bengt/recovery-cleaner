import os
from pathlib import Path
from typing import List


# @cache_to_file(CACHE_PATH / 'get_file_paths.cache')
def get_file_path_strings(folder_path: str) -> List[str]:
    file_path_strings: List[str] = []
    file_count: int = 0
    for root_path, directories, filenames in os.walk(folder_path):
        for filename in filenames:
            file_path_string = str(Path(root_path) / filename)

            file_count += 1
            if not file_count % 10_000:
                print(f'{file_count}th file path: {file_path_string}')

            file_path_strings.append(file_path_string)

    print(f'Found {len(file_path_strings)} files in {folder_path}.')

    return file_path_strings
