import time
from itertools import product
from pathlib import Path

from recovery_cleaner.configuration import Configuration
from recovery_cleaner.deduplicate_library.parse_file_path import \
    parse_date_from_path_parent
from recovery_cleaner.deduplicate_library.print_trashing_duplicate_file \
    import \
    print_moving_duplicate_file
from recovery_cleaner.deduplicate_library.try_extending_path_to_another \
    import \
    try_extending_path_to_another
from recovery_cleaner.duplicate.deduplicate import \
    get_file_paths_to_file_digests
from recovery_cleaner.util.cache_to_file import cache_to_file
from recovery_cleaner.util.paths import CACHE_PATH


def print_digest_and_list_of_duplicate_files(digest, file_path_list):
    print(
        f'Found duplicate files, namely "{digest}" at: \n'
        f'{", ".join(file_path_list)}'
    )


@cache_to_file(CACHE_PATH / 'get_digests_to_file_path_lists.cache')
def get_digests_to_file_path_lists(file_paths_to_digests):
    digests_to_file_path_lists: dict = dict()

    print('Looking for duplicates ...')
    start = time.time()
    for (file_path_string, file_digest), (file_path_string2, file_digest2) in \
            product(file_paths_to_digests.items(), repeat=2):

        if file_digest != file_digest2:
            # These are not duplicate files.
            # So continue with the next file pair.
            continue

        if file_digest not in digests_to_file_path_lists:
            digests_to_file_path_lists[file_digest] = {file_path_string}

        digests_to_file_path_lists[file_digest].add(file_path_string2)

    print(
        f'Looking for duplicates took {time.time() - start} seconds.'
    )

    print('Fossilizing dictionary for caching ...')
    start = time.time()
    for key in digests_to_file_path_lists:
        digests_to_file_path_lists[key] = list(digests_to_file_path_lists[key])
    print(f'Fossilizing dictionary took {time.time() - start} seconds.')

    return digests_to_file_path_lists


def file_path_is_canonical(file_path_string):
    file_path: Path = Path(file_path_string)

    path_parent = file_path.parent

    parse_date_from_path_parent(path_parent)

    file_name = file_path.name
    if not file_name.startswith('IMG_') and \
        not file_name.startswith('DSC_'):
        return False


def deduplicate_files(duplicate_file_path_strings):
    if duplicate_file_path_strings in [
        [
            '/home/bengt/Bilder/2014/08/IMG-20140831-WA0000.jpg',
            '/home/bengt/Bilder/2014/09/01/IMG-20140831-WA0000.jpg',
        ],
        [
            '/home/bengt/Bilder/2013/08/22/IMG_1633 - IMG_1640_blended_fused_sky_cut.png',
            '/home/bengt/Bilder/2014/08/31/IMG_1633 - IMG_1640_blended_fused_sky_cut.png',
        ],
        [
            '/home/bengt/Bilder/2014/08/31/IMG_1633 - IMG_1640_blended_fused_erased.png',
            '/home/bengt/Bilder/2013/08/22/IMG_1633 - IMG_1640_blended_fused_erased.png',
        ],
        [
            '/home/bengt/Bilder/2014/07/22/IMG_20140722_214828504.jpg',
            '/home/bengt/Bilder/2014/07/22.07.14 - 1',
        ],
        [
            '/home/bengt/Bilder/2014/07/22/IMG_20140722_214904486.jpg',
            '/home/bengt/Bilder/2014/07/22.07.14 - 2',
        ],
        [
            '/home/bengt/Bilder/2014/07/CT über vce und co - 1',
            '/home/bengt/Bilder/2014/07/01/IMG_20140701_154445608.jpg'
        ],
        [
            '/home/bengt/Bilder/2014/07/01/IMG_20140701_154425868.jpg',
            '/home/bengt/Bilder/2014/07/CT über vce und co - 3'
        ],
        [
            '/home/bengt/Bilder/2014/07/CT über vce und co - 4',
            '/home/bengt/Bilder/2014/07/01/IMG_20140701_154415278.jpg'
        ],
        [
            '/home/bengt/Bilder/2014/07/01/IMG_20140701_154406554.jpg',
            '/home/bengt/Bilder/2014/07/CT über vce und co - 5'
        ],
        [
            '/home/bengt/Bilder/2014/07/01/IMG_20140701_154432955.jpg',
            '/home/bengt/Bilder/2014/07/CT über vce und co - 2'],
        #[
        #    '/home/bengt/Bilder/2014/09/02/IMG_20140902_103503655_HDR.jpg',
        #    '/home/bengt/Bilder/2014/09/02/IMG_20140902_103503655_HDR - 1rG1Np2ilCy4OsdJH-SllSsivAtgJIKV7Jw.jpg',
        #    '/home/bengt/Bilder/2014/09/IMG_20140902_103503655_HDR - 1rG1Np2ilCy4OsdJH-SllSsivAtgJIKV7Jw.jpg'
        #],
    ]:
        return

    # If any path is canonical.
    if any(
        file_path_is_canonical(file_path_string=path)
        for path in duplicate_file_path_strings
    ):

    for path in duplicate_file_path_strings:

        if not :
            for duplicate_path in duplicate_file_path_strings:
                if duplicate_path is path:
                    continue
                print_moving_duplicate_file(
                    target_file_path=path,
                    source_file_path=duplicate_path
                )
                return

    raise NotImplementedError(
        'No canonical name found.'
    )

    shortest: str = min(duplicate_file_path_strings, key=len)
    shortest_path: Path = Path(shortest)
    path = shortest_path.parent
    name = shortest_path.name
    stem = shortest_path.stem
    suffix = shortest_path.suffix

    longest = max(duplicate_file_path_strings, key=len)
    path_auswahl_name = \
        str(shortest_path.parent / 'auswahl' / shortest_path.name)
    if path_auswahl_name == longest:
        print_moving_duplicate_file(
            target_file_path=longest,
            source_file_path=shortest_path,
        )

        # TODO Actually trash the file.

        return

    path = Path(
        str(path)[:-len('/1970/01/01')]
        if str(path).endswith('/1970/01/01') else path
    )

    favorite = longest
    if ' - ' in suffix:
        print_moving_duplicate_file(
            shortest_path,
            favorite
        )
        return

    extended_path = try_extending_path_to_another(
        duplicate_file_path_strings,
    )
    # TODO Trash original path


def deduplicate_library():
    home_file_paths_to_digests = \
        get_file_paths_to_file_digests(
            folder_path=Configuration.home_images_folder_path,
        )

    digests_to_file_path_lists = get_digests_to_file_path_lists(
        home_file_paths_to_digests
    )

    if len(digests_to_file_path_lists.keys()) == \
        len(home_file_paths_to_digests.keys()):
        print('Found no duplicates. Exiting.')
        exit()
    else:
        print(
            f'Found {len(home_file_paths_to_digests.keys())} files, '
            f'but only {len(digests_to_file_path_lists.keys())} digests. '
            'Will try to deduplicate them.'
        )

    for digest, file_path_list in digests_to_file_path_lists.items():
        if len(file_path_list) == 1:
            # No duplicates
            continue

        print_digest_and_list_of_duplicate_files(digest, file_path_list)

        deduplicate_files(duplicate_file_path_strings=file_path_list)


if __name__ == '__main__':
    print(file_path_is_canonical(
        file_path_string='/home/bengt/Bilder/2014/09/02/IMG_20140902_103503655_HDR.jpg'
    )
    )
    print(file_path_is_canonical(
        file_path_string='/home/bengt/Bilder/2014/09/02/IMG_20140902_103503655_HDR - 1rG1Np2ilCy4OsdJH-SllSsivAtgJIKV7Jw.jpg'
    ))
    print(file_path_is_canonical(
        file_path_string='/home/bengt/Bilder/2014/09/IMG_20140902_103503655_HDR - 1rG1Np2ilCy4OsdJH-SllSsivAtgJIKV7Jw.jpg'
    ))
