import os
import shutil
import time
from datetime import datetime
from itertools import product
from pathlib import Path
from typing import Tuple

from PIL import ExifTags, UnidentifiedImageError
from PIL import Image

from recovery_cleaner.configuration import Configuration
from recovery_cleaner.deduplicate_library.parse_file_path import \
    parse_year_from_path_parent, parse_day_from_path_parent, \
    parse_month_from_path_parent
from recovery_cleaner.deduplicate_library.try_extending_path_to_another \
    import try_extending_path_to_another
from recovery_cleaner.duplicate.deduplicate import \
    get_file_paths_to_file_digests, digest_file
from recovery_cleaner.util.cache_to_file import cache_to_file
from recovery_cleaner.util.paths import CACHE_PATH


def print_digest_and_list_of_duplicate_files(digest, file_path_list):
    print(
        f'Found duplicate files, namely "{digest}" at: \n'
        f'{", ".join(file_path_list)}'
    )


# @cache_to_file(CACHE_PATH / 'get_digests_to_file_path_lists.cache')
def get_digests_to_file_path_lists(file_paths_to_digests):
    digests_to_file_path_lists: dict = dict()

    print('Looking for duplicates ...')
    start = time.time()
    for (file_path_string, file_digest), (file_path_string2, file_digest2) in \
            product(file_paths_to_digests.items(), repeat=2):

        if file_digest != file_digest2:
            # These are not duplicate files.
            # So continue with the next file pair.
            continue

        if file_digest not in digests_to_file_path_lists:
            digests_to_file_path_lists[file_digest] = {file_path_string}

        digests_to_file_path_lists[file_digest].add(file_path_string2)

    print(
        f'Looking for duplicates took {time.time() - start} seconds.'
    )

    print('Fossilizing dictionary for caching ...')
    start = time.time()
    for key in digests_to_file_path_lists:
        digests_to_file_path_lists[key] = list(digests_to_file_path_lists[key])
    print(f'Fossilizing dictionary took {time.time() - start} seconds.')

    return digests_to_file_path_lists


def file_path_is_canonical(path_parent):
    path_parent_parent = path_parent.parent
    path_parent_parent_parent = path_parent.parent.parent
    path_parent_parent_parent_parent = path_parent.parent.parent.parent

    if str(path_parent_parent_parent_parent) != \
            str(Configuration.home_images_folder_path):
        return False

    day_string = \
        str(path_parent)[len(str(path_parent_parent)) + 1:]
    month_string = \
        str(path_parent)[
            len(str(path_parent_parent_parent)) + 1:
            -(len(day_string) + 1)
        ]
    year_string = \
        str(path_parent)[
            len(str(path_parent_parent_parent_parent)) + 1:
            -len('/' + month_string + '/' + day_string)
        ]

    if len(day_string) != 2:
        return False
    # TODO Check more thoroughly

    if len(month_string) != 2:
        return False
    # TODO Check more thoroughly

    if len(year_string) != 4:
        return False
    # TODO Check more thoroughly

    return True


def file_name_is_canonical(file_name):
    # Consider stitched panoramas canonical, regardless of any details.
    if 'blended_fused' in file_name:
        return True

    if str(Path(file_name).stem).startswith('VID_'):
        date_and_time_slugs = \
            str(Path(file_name).stem)[len('VID_'):].split('_')
        date_slug = date_and_time_slugs[0]
        time_slug = date_and_time_slugs[0]

        try:
            int(date_slug)
            int(time_slug)
            return True
        except ValueError:
            # These do not seem to be date and time slugs
            pass

    file_name_extension = Path(file_name).suffix
    if file_name_extension not in Configuration.canonical_file_name_extensions:
        return False

    file_name_base = Path(file_name).stem

    # Manually named-images are canonical.
    only_non_int_characters = True
    for character in file_name_base:
        try:
            int(character)
            # Character parseable, so not exclusively non-ints.
            only_non_int_characters = False
            break
        except ValueError:
            # Character is not parseable, which is fine.
            pass
    if only_non_int_characters:
        return True

    # Names consisting of a Camera-prefix and a running number are canonical.
    camera_prefixes: Tuple[str] = (
        'DSC_',
        'IMG_'
    )
    for camera_prefix in camera_prefixes:
        if file_name_base.startswith(camera_prefix):
            try:
                int(file_name_base[len(camera_prefix):])
                # The prefix-stripped name is indeed a parseable int.
                return True
            except ValueError:
                pass

    return False


def get_canonical_path(duplicate_file_path_strings):
    for duplicate_file_path_string in duplicate_file_path_strings:
        if Path(duplicate_file_path_string).suffix not in \
                Configuration.canonical_file_name_extensions:
            raise ValueError(
                'No canonical path found.'
            )

        Image.MAX_IMAGE_PIXELS = 20_000 * 20_000

        try:
            image: Image = Image.open(duplicate_file_path_string)
        except UnidentifiedImageError:
            raise ValueError('Cannot identify file.')
        except FileNotFoundError:
            raise ValueError('File not found.')

        exif_tags: ExifTags = image.getexif()
        if exif_tags == {}:
            continue
        creator = exif_tags.get(305)
        if creator == 'Picasa':
            possibly_canonical_file_path = \
                str(Path(duplicate_file_path_string.split(' - ')[0])) + \
                Path(duplicate_file_path_string).suffix
            try:
                day = parse_day_from_path_parent(
                    path_parent=Path(possibly_canonical_file_path).parent
                )
                month = parse_month_from_path_parent(
                    path_parent=Path(possibly_canonical_file_path).parent,
                    day_string=day
                )
                year = parse_year_from_path_parent(
                    path_parent=Path(possibly_canonical_file_path).parent,
                    day_string=day,
                    month_string=month,
                )
                return possibly_canonical_file_path

            except ValueError:
                continue

        creation_time = exif_tags.get(306)
        if creation_time is None:
            raise ValueError('No creation time in EXIF data.')
        creation_time = datetime.strptime(creation_time, '%Y:%m:%d %H:%M:%S')
        possibly_canonical_file_path: Path = \
            Path(Configuration.home_images_folder_path) / \
            str(creation_time.year) / \
            str(creation_time.month).zfill(2) / \
            str(creation_time.day).zfill(2) / \
            Path(duplicate_file_path_string).name
        return possibly_canonical_file_path

    raise ValueError(
        'No canonical path found.'
    )


def double_check_move(source_file_path_string, target_file_path_string):
    if not os.path.exists(source_file_path_string):
        raise ValueError()

    if os.path.exists(target_file_path_string):
        digest_source = digest_file(file_path=source_file_path_string)

        digest_target = digest_file(file_path=target_file_path_string)

        # Double check target file path
        if digest_source != digest_target:
            raise ValueError()

    print('Clear for moving.')


def deduplicate_files(duplicate_file_path_strings):
    # If we can derive a canonical path, try using that.
    try:
        canonical_path = str(get_canonical_path(
            duplicate_file_path_strings=duplicate_file_path_strings
        ))
        if canonical_path not in duplicate_file_path_strings and \
                os.path.exists(canonical_path):
            print(
                f'Refusing to overwrite non-duplicate file with another:\n'
                f'Non-duplicate file: {canonical_path}\n'
                f'Other files: {duplicate_file_path_strings}'
            )
        else:
            for source_file_path_string in duplicate_file_path_strings:
                execute_move(
                    canonical_file_path=canonical_path,
                    source_file_path_string=source_file_path_string,
                )
            return
    except ValueError:
        pass

    file_paths: Tuple[Path] = tuple(
        Path(file_path_string).parent
        for file_path_string
        in duplicate_file_path_strings
    )

    file_names: Tuple[str] = tuple(
        str(Path(file_path_string).name)
        for file_path_string
        in duplicate_file_path_strings
    )

    # If any path is canonical, select it.
    for file_path in file_paths:
        for file_name in file_names:
            path_is_canonical = file_path_is_canonical(path_parent=file_path)
            name_is_canonical = file_name_is_canonical(file_name=file_name)
            if path_is_canonical and name_is_canonical:
                canonical_file_path = file_path / file_name
                print(
                    'Found valid combination:\n',
                    canonical_file_path
                )

                for source_file_path_string in duplicate_file_path_strings:
                    execute_move(
                        canonical_file_path=canonical_file_path,
                        source_file_path_string=source_file_path_string,
                    )

                return

    for shortest_file_path_string in duplicate_file_path_strings:
        try:
            extended_path = try_extending_path_to_another(
                shortest_file_path_string=shortest_file_path_string,
                duplicate_file_path_strings=duplicate_file_path_strings,
            )
            print(
                'Moving duplicate file to its extended version:\n'
                f'Source file: {shortest_file_path_string}\n'
                f'Target file: {extended_path}\n'
            )

            execute_move(
                canonical_file_path=extended_path,
                source_file_path_string=shortest_file_path_string,
            )

            return
        except ValueError:
            pass

    if duplicate_file_path_strings in [
        # Wrong date set in camera or something.
        [
            '/home/bengt/Bilder/2014/08/IMG-20140831-WA0000.jpg',
            '/home/bengt/Bilder/2014/09/01/IMG-20140831-WA0000.jpg'
        ],
        (
            '/home/bengt/Bilder/2014/08/IMG-20140831-WA0000.jpg',
            '/home/bengt/Bilder/2014/09/01/IMG-20140831-WA0000.jpg'
        ),
        (
            '/home/bengt/Bilder/2014/09/Bildschirmfoto von »2014-09-29 16:54:59«.png',
            '/home/bengt/Bilder/2015/03/23/Bildschirmfoto von »2014-09-29 16:54:59«.png'
        ),
        (
            '/home/bengt/Bilder/2017/08/24/IMG_0719 - 806 - Wasserfall/Panoramen/IMG_0720 - IMG_0800_v15.jpg',
            '/home/bengt/Bilder/2018/09/08/IMG_0720 - IMG_0800_v15.jpg'
        ),
        (
            '/home/bengt/Bilder/2017/08/24/IMG_0719 - 806 - Wasserfall/Panoramen/IMG_0720 - IMG_0800_v15.jpg',
            '/home/bengt/Bilder/2018/09/08/IMG_0720 - IMG_0800_v15.jpg'
        ),
        (
            '/home/bengt/Bilder/2017/08/24/IMG_0719 - 806 - Wasserfall/Panoramen/IMG_0720 - IMG_0800_v15-03_mod.jpeg',
            '/home/bengt/Bilder/2018/09/09/IMG_0720 - IMG_0800_v15-03_mod.jpeg'
        ),
        (
            '/home/bengt/Bilder/2013/05/11/Mitzeichneranzahl ePetition Vorratsdatenspeicherung - 1Ps43Kk-dTKKnDBtHjmmh-LivfzX3U1UROw.png',
            '/home/bengt/Bilder/2011/09/Mitzeichneranzahl ePetition Vorratsdatenspeicherung - 1Ps43Kk-dTKKnDBtHjmmh-LivfzX3U1UROw.png'
        ),
        (
            '/home/bengt/Bilder/2013/05/11/Mitzeichneranzahl ePetition Vorratsdatenspeicherung - 1ashtwQEeXAZ2ltdjQ7KHaptwOp7L7bvh0A.png',
            '/home/bengt/Bilder/2011/09/Mitzeichneranzahl ePetition Vorratsdatenspeicherung - 1ashtwQEeXAZ2ltdjQ7KHaptwOp7L7bvh0A.png'
        ),
        (
            '/home/bengt/Bilder/2013/05/11/wv30D.png',
            '/home/bengt/Bilder/2011/07/wv30D.png'
        ),
        (
            '/home/bengt/Bilder/2011/07/y74dc.jpg',
            '/home/bengt/Bilder/2013/05/11/y74dc.jpg'
        ),
    ]:
        return

    file_list_formatted = '\n'.join(duplicate_file_path_strings)
    print(
        'WARNING: '
        f'No way of deduplicating these files:\n'
        f'{file_list_formatted}'
    )


def execute_move(
    *,
    canonical_file_path,
    source_file_path_string,
):
    if (
        # The paths differ at least by (not) ending in '_1':
        Path(canonical_file_path).stem.endswith('_1') and
        not Path(source_file_path_string).stem.endswith('_1') and
        # The paths differ only by (not) ending in '_1':
        Path(canonical_file_path).stem[: -len('_1')] +
        Path(canonical_file_path).suffix ==
        str(source_file_path_string)
    ):
        canonical_file_path, source_file_path_string = \
            source_file_path_string, canonical_file_path

    if str(canonical_file_path) == source_file_path_string:
        return

    try:
        double_check_move(
            source_file_path_string=source_file_path_string,
            target_file_path_string=canonical_file_path,
        )
    except ValueError:
        return

    answer = ''
    # answer = input(
    #     'Move this file? [Y/n]:\n'
    #     f'from: {source_file_path_string}\n'
    #     f'  to: {canonical_file_path}'
    # )

    prepare_directories(canonical_file_path)

    if answer in {'y', 'Y', ''}:
        shutil.move(
            src=source_file_path_string,
            dst=canonical_file_path,
        )


def prepare_directories(canonical_file_path):
    try:
        os.mkdir(Path(canonical_file_path).parent.parent.parent)
    except OSError:
        pass
    try:
        os.mkdir(Path(canonical_file_path).parent.parent)
    except OSError:
        pass
    try:
        os.mkdir(Path(canonical_file_path).parent)
    except OSError:
        pass


def deduplicate_library():
    home_file_paths_to_digests = \
        get_file_paths_to_file_digests(
            folder_path=Configuration.home_images_folder_path,
        )

    digests_to_file_path_lists = get_digests_to_file_path_lists(
        home_file_paths_to_digests
    )

    if len(digests_to_file_path_lists.keys()) == \
        len(home_file_paths_to_digests.keys()):
        print('Found no duplicates. Exiting.')
        exit()
    else:
        print(
            f'Found {len(home_file_paths_to_digests.keys())} files, '
            f'but only {len(digests_to_file_path_lists.keys())} digests. '
            'Will try to deduplicate them.'
        )

    for digest, file_path_list in digests_to_file_path_lists.items():
        # These folders serve the purpose of preselecting images for panoramas.
        # So do not deduplicate them.
        file_path_list = tuple(
            duplicate_file_path_string
            for duplicate_file_path_string in file_path_list
            if
                'auswahl' not in duplicate_file_path_string and
                'Foyer' not in duplicate_file_path_string and
                'set1' not in duplicate_file_path_string and
                os.path.exists(duplicate_file_path_string) and
                Path(duplicate_file_path_string).suffix not in ('.log', )
        )

        if len(file_path_list) <= 1:
            # No duplicates
            continue

        print_digest_and_list_of_duplicate_files(digest, file_path_list)

        deduplicate_files(duplicate_file_path_strings=file_path_list)
