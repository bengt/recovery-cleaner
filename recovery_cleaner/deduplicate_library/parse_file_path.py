def parse_date_from_path_parent(path_parent):
    day_string = parse_day_from_path_parent(path_parent)
    month_string = parse_month_from_path_parent(day_string, path_parent)
    year_string = parse_year_from_path_parent(day_string, month_string,
                                              path_parent)

    return year_string, month_string, day_string


def parse_year_from_path_parent(day_string, month_string, path_parent):
    path_parent_parent_parent_parent = path_parent.parent.parent.parent
    year_string = \
        str(path_parent)[
            len(str(path_parent_parent_parent_parent)) + 1:
            -len('/' + month_string + '/' + day_string)
        ]
    if len(year_string) != 4:
        raise ValueError()
    if int(year_string) not in range(1990, 2021):
        raise ValueError()


def parse_month_from_path_parent(day_string, path_parent):
    path_parent_parent_parent = path_parent.parent.parent
    month_string = \
        str(path_parent)[
            len(str(path_parent_parent_parent)) + 1:
            -(len(day_string) + 1)
        ]
    if len(month_string) != 2:
        raise ValueError()
    if int(month_string) not in range(1, 12 + 1):
        raise ValueError()
    return month_string


def parse_day_from_path_parent(path_parent):
    path_parent_parent = path_parent.parent
    day_string = \
        str(path_parent)[len(str(path_parent_parent)) + 1:]
    if len(day_string) != 2:
        raise ValueError()
    if int(day_string) not in range(1, 31 + 1):
        raise ValueError()
    return day_string
