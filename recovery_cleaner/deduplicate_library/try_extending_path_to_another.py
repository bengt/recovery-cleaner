from pathlib import Path

from recovery_cleaner.deduplicate_library.print_trashing_duplicate_file \
    import print_moving_duplicate_file


def try_extending_path_to_another(
    shortest_file_path_string,
    duplicate_file_path_strings,
):
    shortest_path: Path = Path(shortest_file_path_string)
    path = shortest_path.parent
    name = shortest_path.name
    stem = shortest_path.stem
    suffix = shortest_path.suffix

    for year in (None, *range(2001, 2021 + 1)):
        year = str(year) if year else year
        for month in (None, *range(1, 12 + 1)):
            month = str(month).zfill(2) if month else month
            for day in (None, *range(1, 31 + 1)):
                day = str(day).zfill(2) if day else day

                path_extended = ''
                if year is not None and month is not None and day is not None:
                    path_extended = \
                        str(path / year / month / day / name)
                    path_extended_1 = \
                        str(path / year / month / day / (stem + '_1' + suffix))
                    path_extended_1_in_backets = \
                        str(path / year / month / day / (
                                    stem + '(1)' + suffix))
                    path_extended_2 = \
                        str(path / year / month / day / (stem + '_2' + suffix))
                    path_extended_2_in_backets = \
                        str(path / year / month / day / (
                                    stem + '(2)' + suffix))
                    path_extended_3 = \
                        str(path / year / month / day / (stem + '_3' + suffix))
                    path_extended_modified = \
                        str(path / year / month / day / (
                                    stem + '_verändert' + suffix))
                if year is None and month is not None and day is not None:
                    path_extended = \
                        str(path / month / day / name)
                    path_extended_1 = \
                        str(path / month / day / (stem + '_1' + suffix))
                    path_extended_1_in_backets = \
                        str(path / month / day / (stem + '(1)' + suffix))
                    path_extended_2 = \
                        str(path / month / day / (stem + '_2' + suffix))
                    path_extended_2_in_backets = \
                        str(path / month / day / (stem + '(2)' + suffix))
                    path_extended_3 = \
                        str(path / month / day / (stem + '_3' + suffix))
                    path_extended_modified = \
                        str(path / month / day / (
                                    stem + '_verändert' + suffix))
                if year is None and month is None and day is not None:
                    path_extended = \
                        str(path / day / name)
                    path_extended_1 = \
                        str(path / day / (stem + '_1' + suffix))
                    path_extended_1_in_backets = \
                        str(path / day / (stem + '(1)' + suffix))
                    path_extended_2 = \
                        str(path / day / (stem + '_2' + suffix))
                    path_extended_2_in_backets = \
                        str(path / day / (stem + '(2)' + suffix))
                    path_extended_3 = \
                        str(path / day / (stem + '_3' + suffix))
                    path_extended_modified = \
                        str(path / day / (stem + '_verändert' + suffix))
                if year is None and month is None and day is None:
                    # This makes no sense, here.
                    # path_extended = \
                    #     str(path / day / name)
                    path_extended_1 = \
                        str(path / (stem + '_1' + suffix))
                    path_extended_1_in_backets = \
                        str(path / (stem + '(1)' + suffix))
                    path_extended_2 = \
                        str(path / (stem + '_2' + suffix))
                    path_extended_2_in_backets = \
                        str(path / (stem + '(2)' + suffix))
                    path_extended_3 = \
                        str(path / (stem + '_3' + suffix))
                    path_extended_modified = \
                        str(path / (stem + '_verändert' + suffix))

                if path_extended in duplicate_file_path_strings:
                    print_moving_duplicate_file(path_extended, shortest_path)

                    return path_extended

                if path_extended_1 in duplicate_file_path_strings:
                    print_moving_duplicate_file(path_extended_1,
                                                shortest_path)

                    return path_extended_1

                if path_extended_1_in_backets in duplicate_file_path_strings:
                    print_moving_duplicate_file(path_extended_1_in_backets,
                                                shortest_path)

                    return path_extended_1_in_backets

                if path_extended_2 in duplicate_file_path_strings:
                    print_moving_duplicate_file(path_extended_2,
                                                shortest_path)

                    return path_extended_2

                if path_extended_2_in_backets in duplicate_file_path_strings:
                    print_moving_duplicate_file(path_extended_2_in_backets,
                                                shortest_path)

                    return path_extended_2_in_backets

                if path_extended_3 in duplicate_file_path_strings:
                    print_moving_duplicate_file(path_extended_3,
                                                shortest_path)

                    return path_extended_3

                if path_extended_modified in duplicate_file_path_strings:
                    print_moving_duplicate_file(path_extended_modified,
                                                shortest_path)

                    return path_extended_modified

    file_list_formatted = '\n'.join(duplicate_file_path_strings)
    raise ValueError(
        'Could not extend path to any of the others:'
        f'path: {path}'
        f'others: {file_list_formatted}'
    )
