def print_moving_duplicate_file(target_file_path, source_file_path):
    print(
        'Moving duplicate file onto another:\n'
        f'Source: {source_file_path}\n'
        f'Target: {target_file_path}'
    )
