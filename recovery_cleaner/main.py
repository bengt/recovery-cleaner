from recovery_cleaner.deduplicate_library.deduplicate_library import \
    deduplicate_library


def main():
    # deduplicate_files_in_backup()

    # delete_unusable_images()

    # delete_thumbnail_images()

    # move_own_photos()

    deduplicate_library()


if __name__ == '__main__':
    main()
