from pathlib import Path

from recovery_cleaner.deduplicate_library.deduplicate_library import \
    file_path_is_canonical, file_name_is_canonical


def test_deduplicate_library():
    assert not file_path_is_canonical(
        Path('/home/bengt/Bilder/DSC_0138.JPG').parent
    )

    assert file_path_is_canonical(
        Path('/home/bengt/Bilder/2014/08/07/DSC_0138.JPG').parent
    )

    assert file_name_is_canonical(
        Path('/home/bengt/Bilder/DSC_0138.JPG').name
    )
