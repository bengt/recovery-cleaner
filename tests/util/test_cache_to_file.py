from recovery_cleaner.util.cache_to_file import cache_to_file
from recovery_cleaner.util.paths import CACHE_PATH


@cache_to_file(CACHE_PATH / 'cached_function.cache')
def cached_function(x, y):
    print(f'I might be cached! {x}, {y}')


if __name__ == '__main__':
    cached_function(1, 0)
    # print(ROOT_PATH)
    # print(CACHE_PATH)
